#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QSize>
#include <QPoint>
#include <QList>

#define WINDOW_DEFAULT_WIDTH    1100
#define WINDOW_DEFAULT_HEIGHT   750

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(QObject *parent = nullptr);
    static QString getPortName();
    static int getBaudrate();
    static int getDatabits();
    static int getStopbits();
    static int getParity();
    static int getTheme();
    static QStringList getLineColors();
    static QStringList getLineNames();
    static bool hideRecvArea();
    static bool hideRecvData();
    static bool displayRawData();
    static QSize windowSize();
    static QPoint windowPos();
    static QList<int> plotArgs();
    static QString lastSavePath();

    static void setPortName(QString portName);
    static void setBaudrate(int baudrate);
    static void setDatabits(int index);
    static void setStopbits(int index);
    static void setParity(int index);
    static void setTheme(int index);
    static void setLineColors(const QStringList& l);
    static void setLineNames(const QStringList l);
    static void setHideRecvArea(bool enable);
    static void setHideRecvData(bool enable);
    static void setDisplayRawData(bool enable);
    static void setWindowSize(QSize s);
    static void setWindowPos(QPoint p);
    static void setPlotArgs(QList<int> l);
    static void setLastSavePath(const QString & path);
signals:

};

#endif // SETTINGS_H
