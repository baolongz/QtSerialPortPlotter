#include "settingsdialog.h"
#include "ui_settingsdialog.h"
#include "settings.h"
#include <QColorDialog>
#include <QDebug>
#include <QInputDialog>
#include <QMessageBox>

SettingsDialog::SettingsDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    // 设置多选模式
    ui->lineColorslistWidget->setSelectionMode(QListWidget::ExtendedSelection);

    connect(ui->lineColorslistWidget, &QListWidget::itemDoubleClicked, this, [=](QListWidgetItem* item){

        bool ok = false;
        QString channelName = QInputDialog::getText(this, tr("设置通道名称"), tr("名称："), QLineEdit::Normal, item->text(), &ok, Qt::FramelessWindowHint);
        if(ok)
        {
            if(channelName.isEmpty())
            {
                QMessageBox::critical(this, tr("错误"), tr("通道名称不能为空！"));
                return ;
            }

            QColor c = QColorDialog::getColor(item->foreground().color());
            if(c.isValid())
            {
                item->setText(channelName);
                item->setForeground(QBrush(c));
            }
        }

    });

    connect(ui->addButton, &QPushButton::clicked, this, [=](){
        bool ok = false;
        QString channelName = QInputDialog::getText(this, tr("设置通道名称"), tr("名称："), QLineEdit::Normal, "", &ok, Qt::FramelessWindowHint);
        if(ok)
        {
            if(channelName.isEmpty())
            {
                QMessageBox::critical(this, tr("错误"), tr("通道名称不能为空！"));
                return ;
            }

            QColor c = QColorDialog::getColor();
            if(c.isValid())
            {
                ui->lineColorslistWidget->addItem(channelName);
                ui->lineColorslistWidget->item(ui->lineColorslistWidget->count() - 1)->setForeground(QBrush(c));
            }
        }
    });

    connect(ui->removeButton, &QPushButton::clicked, this, [=](){
        while (true)
        {
            QModelIndexList indexs = ui->lineColorslistWidget->selectionModel()->selectedIndexes();
            if(indexs.isEmpty())
                break;
            QListWidgetItem* item = ui->lineColorslistWidget->takeItem(indexs[0].row());
            delete item;
        }
    });

    ui->themeComboBox->addItems({tr("默认"), tr("酷黑")});
}

SettingsDialog::~SettingsDialog()
{
    delete ui;
}

void SettingsDialog::showEvent(QShowEvent *e)
{
    QDialog::showEvent(e);
    ui->themeComboBox->setCurrentIndex(Settings::getTheme());
    QStringList colors = Settings::getLineColors();
    QStringList names = Settings::getLineNames();
    for(int i = 0; i != colors.size(); i++)
    {
        ui->lineColorslistWidget->addItem(names[i]);
        ui->lineColorslistWidget->item(i)->setForeground(QBrush(QColor(colors[i])));
    }
}

void SettingsDialog::accept()
{
    QStringList colors;
    QStringList names;
    for(int i = 0; i != ui->lineColorslistWidget->count(); i++)
    {
        QListWidgetItem* item = ui->lineColorslistWidget->item(i);
        QRgb rgb = item->foreground().color().rgb() & 0XFFFFFFU;
        colors.append(QString::asprintf("#%06X", rgb));
        names.append(item->text());
    }
    Settings::setLineColors(colors);
    Settings::setLineNames(names);
    Settings::setTheme(ui->themeComboBox->currentIndex());
    done(Accepted);
}
